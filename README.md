SSR Security offers security guards, private security, event security, residential security guards, commercial security guards and more in Vancouver, Surrey, Langley, Abbotsford, Coquitlam and surrounding areas.

Address: Head Office: Unit 501 – 3292 Production Way, Burnaby, BC V5A 4R4, Canada

Phone: 604-499-7094

Website: [https://ssrsecurity.ca/](https://ssrsecurity.ca/)
